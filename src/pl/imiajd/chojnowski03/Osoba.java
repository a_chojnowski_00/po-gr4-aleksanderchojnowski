package pl.imiajd.chojnowski03;

import java.time.LocalDate;

public class Osoba implements Comparable<Osoba>, Cloneable {
    public Osoba(String nazwisko, LocalDate DataUrodzenia) {
        this.nazwisko = nazwisko;
        this.DataUrodzenia = DataUrodzenia;
    }

    public String getNazwisko() {
        return "Nazwisko: " + nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return DataUrodzenia;
    }


    private String nazwisko;
    private LocalDate DataUrodzenia;

    @Override
    public int compareTo(Osoba other) {
        int wynik= this.nazwisko.compareTo(other.nazwisko);
        if(wynik==0){
            return this.DataUrodzenia.compareTo(other.DataUrodzenia);
        }
        return wynik;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"Nazwisko: "+this.nazwisko+"\nData urodzenia: "+this.getDataUrodzenia()+"\n";
    }

    @Override
    public boolean equals(Object obj) {
        Osoba osoba = (Osoba) obj;
        return (osoba.nazwisko.equals(this.nazwisko) && osoba.DataUrodzenia.equals(this.DataUrodzenia));
    }
}