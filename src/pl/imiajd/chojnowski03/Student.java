package pl.imiajd.chojnowski03;

import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable<Osoba> {

    private double sredniaOcen;

    public Student(String nazwisko, LocalDate DataUrodzenia,double sredniaOcen) {
        super(nazwisko, DataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }


    public int compareTo(Osoba o) {
        int ostatni = super.compareTo((o));
        if((o instanceof Student) && (ostatni == 0)){
            return -(int)Math.ceil(this.sredniaOcen-((Student) o).sredniaOcen);
        }
        return ostatni;
    }

    public String toString() {
        return getClass().getSimpleName() + " nazwisko = " + getNazwisko() + ", data Urodzenia =  " + getDataUrodzenia() + ", srednia ocen= "+ sredniaOcen;

    }
}