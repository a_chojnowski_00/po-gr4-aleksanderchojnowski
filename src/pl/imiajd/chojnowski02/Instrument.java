package pl.imiajd.chojnowski02;

import java.time.LocalDate;

public abstract class Instrument{
    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent=producent;
        this.rokProdukcji=rokProdukcji;
    }

    public String getProducent(){
        return producent;
    }

    public LocalDate getRokProdukcji(){
        return rokProdukcji;
    }

    public abstract String getDzwiek();

    @Override
    public String toString() {
        return this.getClass().getSimpleName()+"Producent: "+this.producent+"\nData produkcji: "+this.rokProdukcji+"\n";
    }

    @Override
    public boolean equals(Object obj) {
        return this.toString().equals(obj.toString());
    }


    private String producent;
    private LocalDate rokProdukcji;
}