package pl.imiajd.chojnowski02;

import java.time.LocalDate;

public class Pracownik extends Osoba {
    public Pracownik(String nazwisko, String[] imiona, LocalDate DataUrodzenia, boolean plec, double pobory, LocalDate DataZatrudnienia) {
        super(nazwisko, imiona, DataUrodzenia, plec);
        this.pobory = pobory;
        this.DataZatrudnienia = DataZatrudnienia;
    }

    public double getPobory(){
        return pobory;
    }
    public LocalDate getDataZatrudnienia(){
        return DataZatrudnienia;
    }

    public String getOpis() {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    private double pobory;
    private LocalDate DataZatrudnienia;
}