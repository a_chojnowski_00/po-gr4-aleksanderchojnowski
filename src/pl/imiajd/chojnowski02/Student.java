package pl.imiajd.chojnowski02;

import java.time.LocalDate;

public class Student extends Osoba {
    public Student(String nazwisko, String[] imiona, LocalDate DataUrodzenia, boolean plec, String kierunek, double SredniaOcen)
    {
        super(nazwisko,imiona,DataUrodzenia,plec);
        this.kierunek = kierunek;
    }

    public String getOpis()
    {
        return "kierunek studiów: " + kierunek;
    }
    public String getKierunek() {
        return kierunek;
    }
    public Double getSredniaOcen() {
        return SredniaOcen;
    }
    public void setSredniaOcen(Double srednia) {
        this.SredniaOcen=srednia;
    }

    private double SredniaOcen;
    private String kierunek;
}