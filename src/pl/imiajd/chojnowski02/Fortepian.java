package pl.imiajd.chojnowski02;

import java.time.LocalDate;

public class Fortepian extends Instrument{
    public Fortepian(String producent, LocalDate rokProdukcji){
        super(producent,rokProdukcji);
    }

    @Override
    public String getDzwiek(){
        return "brzęg";
    }
}