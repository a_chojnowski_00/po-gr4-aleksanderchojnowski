package pl.imiajd.chojnowski02;

import java.time.LocalDate;

public abstract class Osoba
{
    public Osoba(String nazwisko, String[] imiona, LocalDate DataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.plec = plec;
        this.DataUrodzenia = DataUrodzenia;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return "Nazwisko: " + nazwisko;
    }
    public String[] getImiona()
    {
        return imiona;
    }
    public String getPlec()
    {
        if(this.plec=true)
            return "Męzczyzna";
        else
            return "Kobieta";
    }
    public LocalDate getDataUrodzenia()
    {
        return DataUrodzenia;
    }

    private String nazwisko;
    private String[] imiona;
    private LocalDate DataUrodzenia;
    private boolean plec;
}