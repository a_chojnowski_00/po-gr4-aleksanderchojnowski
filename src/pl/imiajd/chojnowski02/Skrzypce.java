package pl.imiajd.chojnowski02;

import java.time.LocalDate;

public class Skrzypce extends Instrument{
    public Skrzypce(String producent, LocalDate rokProdukcji){
        super(producent,rokProdukcji);
    }

    @Override
    public String getDzwiek(){
        return "ijoijo";
    }
}