package pl.imiajd.chojnowski02;

import java.time.LocalDate;

public class Flet extends Instrument{
    public Flet(String producent, LocalDate rokProdukcji){
        super(producent,rokProdukcji);
    }

    @Override
    public String getDzwiek(){
        return "fjufju";
    }
}