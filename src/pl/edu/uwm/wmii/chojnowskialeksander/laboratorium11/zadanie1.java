package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium11;
import java.util.PriorityQueue;
import java.util.Scanner;

public class zadanie1 {
    public static void main(String[] args) {
        PriorityQueue<klasaPomocnicza> pq = new PriorityQueue<>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj co chcesz zrobic: ");
        String r = sc.nextLine();
        String opis;
        int priorytet;
        while (!r.equals("zakoncz")){
            if (r.equals("Dodaj priorytet opis")){
                System.out.println("Podaj opis: ");
                opis = sc.nextLine();
                System.out.println("Podaj priorytet: ");
                priorytet = sc.nextInt();
                pq.add(new klasaPomocnicza(opis,priorytet));
            }
            else if (r.equals("nastepne") && pq.size()!=0){
                System.out.println(pq.poll().toString());
            }
            else{
                System.out.println("Dostepne opcje: Dodaj priorytet opis, nastepne, zakoncz");
            }
            System.out.println("Podaj co chcesz zrobic: ");
            r = sc.nextLine();
        }

    }
}
