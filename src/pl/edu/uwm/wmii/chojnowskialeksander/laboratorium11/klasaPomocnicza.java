package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium11;

public class klasaPomocnicza implements Comparable<klasaPomocnicza>{
    private String opis;
    private int priorytet;

    public klasaPomocnicza(String opis, int priorytet) {
        this.opis = opis;
        this.priorytet = priorytet;
    }
    @Override
    public String toString() {
        return "zadaniePom{" +
                "opis='" + opis + '\'' +
                ", priorytet=" + priorytet +
                '}';
    }
    @Override
    public int compareTo(klasaPomocnicza o) {
        if (priorytet < o.priorytet)
            return -1;
        else if (priorytet > o.priorytet)
            return 1;
        return 0;
    }
}
