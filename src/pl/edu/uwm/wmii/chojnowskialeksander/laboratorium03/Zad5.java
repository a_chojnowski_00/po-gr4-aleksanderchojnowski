package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium03;

import java.math.BigDecimal;
import java.math.MathContext;

public class Zad5 {
    public static BigDecimal kapital(double k, double p, int n) {
        BigDecimal wynik = new BigDecimal(k);
        MathContext pom = new MathContext(2);
        for (int i = 0; i < n; i++) {
            BigDecimal temp = wynik.multiply(BigDecimal.valueOf(p), pom);
            wynik = wynik.add(temp);
        }
        return wynik;
    }
}
