package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium03;

import java.util.Arrays;

public class
Zad1 {
    public static int countChar(String str, char c){
        int wynik=0;
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)==c)
                wynik++;
        }
        return wynik;
    }

    public static int countSubStr(String str, String subStr){
        int wynikB=0;
        int n=0;
        for(int i=0;i<=str.length()-subStr.length();i++){
            if(str.charAt(i)==subStr.charAt(0)){
                for(int x=0;x<subStr.length();x++){
                    if(str.charAt(i+x)==subStr.charAt(x)){
                        n++;
                    }
                }
                if(n==subStr.length())
                    wynikB++;
                n=0;
            }
        }
        return wynikB;
    }

    public static String middle(String str){
        String mid="";
        int dlugosc = str.length();
        if(dlugosc==0 || dlugosc==1)
            return str; // w tym przypadku jesli napis ma 1 znak lub 0 to jego srodek jest tym napisem całym
        else if(dlugosc%2==0){
            mid+=str.charAt((dlugosc/2)-1);
            mid+=str.charAt(dlugosc/2);
        }
        else
            mid+=str.charAt(dlugosc/2);
        return mid;
    }

    public static String repeat(String str, int n){
        String wynik="";
        for(int i=0;i<n;i++){
            wynik+=str;
        }
        return wynik;
    }

    public static int[] where(String str, String subStr) {
        int[] wynik = new int[0];
        int n=0;
        for(int i=0;i<=str.length()-subStr.length();i++){
            if(str.charAt(i)==subStr.charAt(0)){
                for(int x=0;x<subStr.length();x++){
                    if(str.charAt(i+x)==subStr.charAt(x)){
                        n++;
                    }
                }
                if(n==subStr.length()) {
                    wynik= Arrays.copyOf(wynik,wynik.length+1);
                    wynik[wynik.length-1]=i;
                }
                n=0;
            }
        }
        return wynik;
    }

    public static String change(String str){
        char[] tab = new char[str.length()];
        StringBuffer buffer = new StringBuffer(str.length());
        for(int i=0; i<str.length();i++){
            tab[i]=str.charAt(i);
        }
        for(int i=0;i<str.length();i++){
            if(Character.isUpperCase(tab[i]))
                buffer.append(Character.toLowerCase(tab[i]));
            else
                buffer.append(Character.toUpperCase(tab[i]));
        }
        return buffer.toString();
    }

    public static String nice(String str){
        StringBuffer buffer = new StringBuffer();
        StringBuffer buffer2 = new StringBuffer();
        int trzy=0;
        for(int i=str.length()-1;i>=0;i--){
            if(trzy==3) {
                buffer.append(";" + str.charAt(i));
                trzy=1;
            }
            else {
                buffer.append(str.charAt(i));
                trzy++;
            }
        }
        for(int i=buffer.length()-1;i>=0;i--){
            buffer2.append(buffer.charAt(i));
        }
        return buffer2.toString();
    }
}
