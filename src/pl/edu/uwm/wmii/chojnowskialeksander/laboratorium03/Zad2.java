package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium03;

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

public class Zad2 {
    public static int countCharInFile(String path, char c){
        try {
            int wynik = 0;
            File plik = new File(path);
            String line;
            Scanner scanner = new Scanner(plik);
            while(scanner.hasNextLine()){
                line = scanner.nextLine();
                wynik+=Zad1.countChar(line,c);
            }
            return wynik;
        }
        catch(FileNotFoundException e){
            System.out.println("Wrong file path");
            e.printStackTrace();
            return 0;
        }
    }
}
