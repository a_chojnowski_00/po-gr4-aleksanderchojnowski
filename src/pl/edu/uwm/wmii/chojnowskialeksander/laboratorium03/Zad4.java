package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium03;

import java.math.BigInteger;

public class Zad4 {
    public static void szachownica(int n){
        BigInteger wynik = new BigInteger("0");
        BigInteger pom = new BigInteger("2");
        for(int i=0; i<n*n;i++){
            wynik=wynik.add(pom.pow(i));
        }
        System.out.println("Na szachownicy jest w sumie " + wynik + " ziaren maku");
    }
    //alternatywne rozwiazanie to: wynik=(2^n*n)-1
}
