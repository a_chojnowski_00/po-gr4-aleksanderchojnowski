package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium03;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zad3 {
    public static int countStringInFile(String path, String str) {
        try {
            int wynik = 0;
            File plik = new File(path);
            String line;
            Scanner scanner = new Scanner(plik);
            while (scanner.hasNextLine()) {
                line = scanner.nextLine();
                wynik += Zad1.countSubStr(line, str);
            }
            return wynik;
        } catch (FileNotFoundException e) {
            System.out.println("Wrong file path");
            e.printStackTrace();
            return 0;
        }
    }
}
