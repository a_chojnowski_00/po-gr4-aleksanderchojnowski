package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium03;

import java.util.Arrays;

public class mainTest {
    static public void main( String [] args ){
//            na potrzeby testów ustawiłem wszelkie napisy na stałe
//            możliwy sposób jest też aby użytkownik wpisywał napisy z
//            klawiatury, jednak to zwiększa czas testowania


        // ZADANIE 1
        String napis ="Ala ma kota a KOT jest RuDY a ala ma na ImIEEeE ala";
        System.out.println(Zad1.countChar(napis,'a'));
        System.out.println(Zad1.countSubStr(napis,"ala"));
        System.out.println(Zad1.middle("testxowy"));
        System.out.println(Zad1.repeat("ho",5));
        System.out.println(Arrays.toString(Zad1.where(napis, "ala")));
        System.out.println(Zad1.change(napis));
        System.out.println(Zad1.nice("7439857489367984723985672"));

        // ZADANIE 2
        System.out.println(Zad2.countCharInFile("src/pl/edu/uwm/wmii/chojnowskialeksander/laboratorium03/lokomotywa.txt",'a'));

        // ZADANIE 3
        System.out.println(Zad3.countStringInFile("src/pl/edu/uwm/wmii/chojnowskialeksander/laboratorium03/wyrazy.txt","wyraz"));

        // ZADANIE 4
        Zad4.szachownica(3);

        // ZADANIE 5
        System.out.println(Zad5.kapital(5000, 0.01, 5));
    }
}
