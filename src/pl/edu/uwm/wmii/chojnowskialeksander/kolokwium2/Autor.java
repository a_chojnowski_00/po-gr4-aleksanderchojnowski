package pl.edu.uwm.wmii.chojnowskialeksander.kolokwium2;

public class Autor implements Cloneable, Comparable<Autor> {
    private String nazwa;
    private String adresEmail;
    char plec;

    public Autor(String nazwa,String adresEmail,char plec) {
        this.nazwa = nazwa;
        this.adresEmail = adresEmail;
        this.plec = plec;
    }

    // Sety i gety
    public String getNazwa(){
        return this.nazwa;
    }
    public void setNazwa(String nazwa){
        this.nazwa = nazwa;
    }
    public String getAdresEmail(){
        return this.nazwa;
    }
    public void setAdresEmail(String nazwa){
        this.nazwa = nazwa;
    }
    public String getPlec(){
        return this.nazwa;
    }
    public void setPlec(String nazwa){
        this.nazwa = nazwa;
    }

    //2 podpunkt
    @Override
    public int compareTo(Autor autor) {
        int compare_nazwa = this.nazwa.compareTo(autor.nazwa);
        if(compare_nazwa==0){
            return this.plec-autor.plec;
        }
        return compare_nazwa;
    }

    public Autor clone() {
        try {
            return (Autor) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError();
        }
    }

    @Override
    public String toString()
    {
        return this.getClass().getSimpleName()+"[Autor: " + this.nazwa+" Adres email: " + this.adresEmail + " Płeć: " + this.plec + "]";
    }
}