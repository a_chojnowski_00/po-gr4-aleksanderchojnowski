package pl.edu.uwm.wmii.chojnowskialeksander.kolokwium2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

public class KolokwiumTest {
    public static void main (String[] args){
        ArrayList<Autor> authors = new ArrayList<>();
        authors.add(0, new Autor("Michał","michal@wp.pl",'m'));
        authors.add(1, new Autor("Michał","michal123456@wp.pl",'m'));
        authors.add(2, new Autor("Tom","tom@gmail.com",'k'));
        authors.add(3, new Autor("Jessica","jessica@o2.pl",'k'));

        System.out.println("\tAutorzy przed sortowaniem:");
        for (Autor autor : authors) {
            System.out.println(autor.toString());
        }
        Collections.sort(authors);
        System.out.println("\tAutorzy po sortowaniem po nazwie:");
        for (Autor author : authors) {
            System.out.println(author.toString());
        }

        System.out.println("\n\n");

        ArrayList<Ksiazka> Listaksiazek = new ArrayList<>();
        Listaksiazek.add(0, new Ksiazka("Podlasie",authors.get(0),19.5));
        Listaksiazek.add(1, new Ksiazka("Photography",authors.get(0),23.59));
        Listaksiazek.add(2, new Ksiazka("Grzybobranie Małgosi",authors.get(1),23.99));
        Listaksiazek.add(3, new Ksiazka("Tutorial",authors.get(2),10.79));

        System.out.println("\tLista książek przed Sortowaniem:");
        for (Ksiazka value : Listaksiazek) {
            System.out.println(value.toString());
        }
        Collections.sort(Listaksiazek);
        System.out.println("\tLista książek po Sortowaniu:");
        for (Ksiazka ksiazka : Listaksiazek) {
            System.out.println(ksiazka.toString());
        }

        System.out.println("\n\n");

        LinkedList<Ksiazka> books = new LinkedList<Ksiazka>();
        books.add(0, new Ksiazka("Podlasie",authors.get(0),19.5));
        books.add(1, new Ksiazka("Photography",authors.get(0),23.59));
        books.add(2, new Ksiazka("Grzybobranie Małgosi",authors.get(1),23.99));
        books.add(3, new Ksiazka("Tutorial",authors.get(2),10.79));
        System.out.println("\tLinkedList books przed usuwaniem:");
        //System.out.println(books);
        for(int i = 0 ; i < books.size() ; i++)
        {
            System.out.println(books.get(i).toString());
        }

        Redukuj.redukuj(books, 2);
        System.out.println("\tLinkedList books po usuwaniu:");
        //System.out.println(books);
        for(int i = 0 ; i < books.size() ; i++)
        {
            System.out.println(books.get(i).toString());
        }
    }
}
