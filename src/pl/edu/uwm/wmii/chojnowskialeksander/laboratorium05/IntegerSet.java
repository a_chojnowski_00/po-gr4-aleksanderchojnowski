package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium05;

public class IntegerSet {
    private boolean[] tablica;
    public IntegerSet(){
        this.tablica=new boolean[100];
    }

    public static IntegerSet unin(IntegerSet a, IntegerSet b){
        IntegerSet wynik = new IntegerSet();
        for(int i=0;i<100;i++){
            if(a.tablica[i] || b.tablica[i]){
                wynik.tablica[i] = true;
            }
        }
        return wynik;
    }

    public static IntegerSet intersection(IntegerSet a, IntegerSet b){
        IntegerSet wynik = new IntegerSet();
        for(int i=0;i<100;i++){
            if(a.tablica[i] && b.tablica[i]){
                wynik.tablica[i] = true;
            }
        }
        return wynik;
    }

    public void insertElement(int n){
        this.tablica[n-1] = true;
    }

    public void deleteElement(int n){
        this.tablica[n-1] = false;
    }

    @Override
    public String toString(){
        String wynik = "";
        for(int i = 0; i<100; i++)
            if(tablica[i] == true)
                wynik += Integer.toString(i+1) + " ";
        if(wynik.length() > 0)
            wynik = wynik.substring(0, wynik.length()-1);
        return wynik;
    }

    public boolean equal(IntegerSet a){
        for(int i=0;i<100;i++){
            if(a.tablica[i]!=this.tablica[i])
                return false;
        }
        return true;
    }



    public boolean[] getTablica(){
        return tablica;
    }
}
