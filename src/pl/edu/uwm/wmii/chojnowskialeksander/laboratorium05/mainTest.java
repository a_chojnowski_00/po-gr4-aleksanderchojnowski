package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium05;

public class mainTest {
    public static void main(String [] args){
        //Zadanie1
        RachunekBankowy saver1 = new RachunekBankowy(2000);
        RachunekBankowy saver2 = new RachunekBankowy(3000);
        saver1.setRocznaStopaProcentowa(0.04);
        saver2.setRocznaStopaProcentowa(0.04);
            saver1.obliczMiesieczneOdsetki();
            saver2.obliczMiesieczneOdsetki();
                System.out.println("po 1 miesiącu: " + saver1.getSaldo());
                System.out.println("po 1 miesiącu: " + saver2.getSaldo());

        saver1.setRocznaStopaProcentowa(0.05);
        saver2.setRocznaStopaProcentowa(0.05);
            saver1.obliczMiesieczneOdsetki();
            saver2.obliczMiesieczneOdsetki();
                System.out.println("po 2 miesiącu: " + saver1.getSaldo());
                System.out.println("po 2 miesiącu: " + saver2.getSaldo());

        //Zadanie2
        IntegerSet tab1 = new IntegerSet();
            tab1.insertElement(12);
            tab1.insertElement(31);
            tab1.insertElement(65);
            tab1.insertElement(63);

        IntegerSet tab2 = new IntegerSet();
            tab2.insertElement(98);
            tab2.insertElement(75);
            tab2.insertElement(45);
            tab2.insertElement(14);
            tab2.insertElement(12);

        System.out.println("tab1: " + tab1.toString());
        System.out.println("Union: " + IntegerSet.unin(tab1,tab2).toString());
        System.out.println("Intersection: " + IntegerSet.intersection(tab1,tab2).toString());
        System.out.println("Czy tab1 i tab2 są takie same?: " + tab1.equal(tab2));

        //Zadanie3
        Pracownik[] personel = new Pracownik[3];

        // wypełnij tablicę danymi pracowników
        personel[0] = new Pracownik("Karol Cracker", 75000, 2001, 12, 15);
        personel[1] = new Pracownik("Henryk Hacker", 50000, 2003, 10, 1);
        personel[2] = new Pracownik("Antoni Tester", 40000, 2005, 3, 15);

        // zwiększ pobory każdego pracownika o 20%
        for (Pracownik e : personel) {
            e.zwiekszPobory(20);
        }

        // wypisz informacje o każdym pracowniku
        for (Pracownik e : personel) {
            System.out.print("nazwisko = " + e.nazwisko() + "\tid = " + e.id());
            System.out.print("\tpobory = " + e.pobory());
            System.out.printf("\tdataZatrudnienia = %tF\n", e.dataZatrudnienia());
        }
        System.out.println();

        int n = Pracownik.getNextId(); // wywołanie metody statycznej
        System.out.println("Następny dostępny id = " + n);
    }
}
