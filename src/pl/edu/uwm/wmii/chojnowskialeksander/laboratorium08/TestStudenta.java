package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium08;

import pl.imiajd.chojnowski03.Student;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudenta {
    public static void main(String[] args) {
        ArrayList<Student> Grupa = new ArrayList<>();
        Grupa.add(0, new Student("Chojnowski", LocalDate.of(2000, 3, 26),4.23));
        Grupa.add(1, new Student("Kowalski", LocalDate.of(1999, 10, 15),2.3));
        Grupa.add(2, new Student("Nowak", LocalDate.of(1456, 12, 25),5));
        Grupa.add(3, new Student("Janczyk", LocalDate.of(645, 9, 12),5));
        Grupa.add(4, new Student("Wojciechowski", LocalDate.of(3251, 8, 12),4.59));
        Grupa.add(5, new Student("Wojciechowski", LocalDate.of(3251, 8, 12),4.6));

        System.out.println("Przed Sortowaniem");
        for(int i = 0 ; i < 6 ; i++)
        {
            System.out.println(Grupa.get(i).toString());
        }
        System.out.println(Grupa.get(2).compareTo(Grupa.get(4)));
        System.out.println(Grupa.get(1).compareTo(Grupa.get(3)));
        Collections.sort(Grupa);
        System.out.println("Po Sortowaniu");
        for(int i = 0 ; i < 5 ; i++)
        {
            System.out.println(Grupa.get(i).toString());
        }
    }
}