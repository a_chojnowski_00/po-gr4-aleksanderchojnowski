package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium08;

import pl.imiajd.chojnowski03.Osoba;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba {
    public static void main(String[] args) {
        ArrayList<Osoba> Grupa = new ArrayList<>();
        Grupa.add(0, new Osoba("Chojnowski", LocalDate.of(2000, 3, 26)));
        Grupa.add(1, new Osoba("Kowalski", LocalDate.of(1999, 10, 15)));
        Grupa.add(2, new Osoba("Nowak", LocalDate.of(1456, 12, 25)));
        Grupa.add(3, new Osoba("Janczyk", LocalDate.of(645, 9, 12)));
        Grupa.add(4, new Osoba("Wojciechowski", LocalDate.of(3251, 8, 12)));

        System.out.println("Przed Sortowaniem");
        for(int i = 0 ; i < 5 ; i++)
        {
            System.out.println(Grupa.get(i).toString());
        }
        System.out.println(Grupa.get(2).compareTo(Grupa.get(4)));
        System.out.println(Grupa.get(1).compareTo(Grupa.get(3)));
        Collections.sort(Grupa);
        System.out.println("Po Sortowaniu");
        for(int i = 0 ; i < 5 ; i++)
        {
            System.out.println(Grupa.get(i).toString());
        }
    }
}