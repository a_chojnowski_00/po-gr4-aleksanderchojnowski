package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium02;

public class Zad2f {
    public static void signum(int[] tab) {
        System.out.print("Funkcja signum od elementów: ");
        for(int elem : tab){
            if(elem>0)
                elem=1;
            else if(elem<0)
                elem=-1;
            System.out.print(elem + " ; ");
        }
    }
}
