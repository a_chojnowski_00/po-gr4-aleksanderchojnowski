package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium02;
import java.util.Random;

public class Zad2a {
    public static void ileNieparzystych(int[] tab) {
        int npar=0;
        for(int elem: tab){
            if(!(elem%2==0)){
                npar++;
            }
        }
        System.out.println("nieparzystych: " +  npar);
    }

    public static void ileParzystych(int[] tab) {
        int par=0;
        for(int elem:tab){
            if(elem%2==0){
                par++;
            }
        }
        System.out.println("parzystych: " + par);
    }
}
