package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium02;

import java.util.Random;

public class FunkcjePomocnicze {
    public static  int getRandomNumber(int min, int max){
        Random random = new Random();
        return random.nextInt(max-min)+min;
    }

    public static void generuj (int[] tab, int n, int min, int max){
        for (int i=0;i<n;i++){
            tab[i] = getRandomNumber(min,max);
            System.out.print(tab[i]);
            if(i<n-1)
                System.out.print(" ; ");
        }
        System.out.println("");
    }
}
