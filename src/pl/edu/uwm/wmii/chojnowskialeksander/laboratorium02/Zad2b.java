package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium02;

public class Zad2b {
    public static void ileDodatnich(int[] tab) {
        int plus=0;
        for(int elem:tab){
            if(elem>0){
                plus++;
            }
        }
        System.out.println("Dodatnich: " + plus);
    }

    public static void ileUjemnych(int[] tab) {
        int minus=0;
        for(int elem:tab){
            if(elem<0){
                minus++;
            }
        }
        System.out.println("Ujemnych: " + minus);
    }

    public static void ileZerowych(int[] tab) {
        int wynik=0;
        for(int elem:tab){
            if(elem==0){
                wynik++;
            }
        }
        System.out.println("Zerowych: " + wynik);
    }

}
