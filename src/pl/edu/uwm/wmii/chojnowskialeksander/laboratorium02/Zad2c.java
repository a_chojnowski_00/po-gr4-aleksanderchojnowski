package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium02;

public class Zad2c {
    public static void ileMaksymalnych(int[] tab){
        int max=tab[0];
        for(int elem: tab){
            if(elem>max)
                max=elem;
        }
        int ilosc=0;
        for(int elem: tab){
            if(elem==max) {
                ilosc++;
            }
        }
        System.out.println("element maxymalny: " + max + " występuje on: " + ilosc + " razy");
    }
}
