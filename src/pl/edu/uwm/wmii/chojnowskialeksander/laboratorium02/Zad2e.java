package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium02;

public class Zad2e {
    public static void dlugoscMaksymalnegoCiaguDodatnich(int[] tab) {
        int max = 0;
        int maxLok = 0;
        for (int elem : tab) {
            if(elem>0) {
                maxLok++;
                if (maxLok > max) {
                    max = maxLok;
                }
            }
            else
                maxLok = 0;
        }
        System.out.println("najdluzszy fragment dodatni ma dlugosc: " + max);
    }
}
