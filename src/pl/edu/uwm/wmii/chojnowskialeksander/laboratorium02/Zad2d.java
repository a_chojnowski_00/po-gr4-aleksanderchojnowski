package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium02;

public class Zad2d{
    public static void sumaUjemnych(int[] tab) {
        int wynik=0;
        for(int elem: tab){
            if(elem<0){
                wynik+=elem;
            }
        }
        System.out.println("Suma elementów ujemncyh: "  + wynik);
    }

    public static void sumaDodatnich(int[] tab) {
        int wynik=0;
        for(int elem: tab){
            if(elem>0){
                wynik+=elem;
            }
        }
        System.out.println("Suma elementów dodatnich: "  + wynik);
    }
}
