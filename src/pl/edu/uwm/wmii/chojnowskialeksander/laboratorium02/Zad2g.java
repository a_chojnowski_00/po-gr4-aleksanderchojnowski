package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium02;

public class Zad2g {
    public static void odwrocFragment(int tab[], int lewy, int prawy) {
        int n= tab.length;
        for (int i = 0; i <= (prawy - lewy) / 2; i++) {
            int bufor = tab[prawy - i];
            tab[prawy - i] = tab[lewy + i];
            tab[lewy + i] = bufor;
        }

        for (int i = 0; i < n; i++) {
            System.out.print(tab[i] + " ; ");
        }
    }
}
