package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium02;
import java.util.Random;
import java.util.Scanner;

public class Zadanie1Test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] tab = new int[size];
        Random random = new Random();


        for (int i=0;i<size;i++){
            int liczba = random.nextInt(999+999)-999;
            tab[i] = liczba;
            System.out.print(tab[i]);
            if(i<size-1)
                System.out.print(" ; ");
        }
        System.out.println("");

        //Podpunkt 1a
        int par = 0;
        int npar = 0;

        for(int i: tab){
            if(i%2==0)
                par++;
            else
                npar++;
        }
        System.out.println("parzystych: " + par);
        System.out.println("nieparzystych: " + npar);


        //Podpunkt 1b
        int minus = 0 ;
        int plus = 0 ;
        int zera = 0 ;
        for(int elem: tab){
            if(elem > 0)
                plus++;
            else if(elem < 0)
                minus++;
            else
                zera++;
        }
        System.out.println("Dodatnich: " + plus);
        System.out.println("ujemnych: " + minus);
        System.out.println("zer: " + zera);


        //Podpunkt 1c
        int max=tab[0];
        for(int elem: tab){
            if(elem>max)
                max=elem;
        }
        int ilosc=0;
        for(int elem: tab){
            if(elem==max) {
                ilosc++;
            }
        }
        System.out.println("element maxymalny: " + max + ", występuje on: " + ilosc + " razy");


        //Podpunkt 1d
        int sumaPlus = 0;
        int sumaMinus = 0;
        for(int elem: tab){
            if(elem>0)
                sumaPlus+=elem;
            else
                sumaMinus+=elem;
        }
        System.out.println("Suma elementów dodarnich: "  + sumaPlus);
        System.out.println("Suma elementów ujemnych: "  + sumaMinus);


        //Podunkt 1e
        int maxdl = 0; //maxymalna dlugosc ostateczna
        int maxLok = 0; //maksymalna dlugosc lokalna
        for (int elem : tab) {
            if(elem>0) {
                maxLok++;
                if (maxLok > maxdl) {
                    maxdl = maxLok;
                }
            }
            else
                maxLok = 0;
        }
        System.out.println("najdluzszy fragment dodatni ma dlugosc: " + maxdl);


        //Podpunkt 1f
        System.out.println("Zamiana dodatnich elemntów na 1 i ujemnych na -1");
        for(int elem : tab){
            if(elem>0)
                elem=1;
            else if(elem<0)
                elem=-1;
            System.out.print(elem + " ; ");
        }



        //Podpunkt g
        System.out.println("Podaj przedział gdzie fragment tablicy ma byc odwrocony: ");
        int lewy = scanner.nextInt();
        int prawy = scanner.nextInt();

        for(int i = 0; i <= (prawy-lewy) / 2; i++)
        {
            int bufor=tab[prawy-i];
            tab[prawy-i]=tab[lewy+i];
            tab[lewy+i]=bufor;
        }

        for(int i = 0; i < size; i++){
            System.out.print(tab[i] + " ; ");
        }
    }
}
