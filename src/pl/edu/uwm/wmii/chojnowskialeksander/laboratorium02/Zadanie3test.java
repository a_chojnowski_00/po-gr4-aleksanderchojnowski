package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium02;

import java.util.Scanner;

public class Zadanie3test {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        int k = scanner.nextInt();

        int[][] macierzA = new int[m][n];
        int[][] macierzB = new int[n][k];

        //Przypisanie wartosci do macierzy A
        for(int i=0;i<m;i++){
            for(int x=0;x<n;x++){
                macierzA[i][x]=FunkcjePomocnicze.getRandomNumber(1,9);
            }
        }
        //Wypisanie macierzy A
        System.out.println("Macierz A: ");
        for(int i=0;i<m;i++){
            for(int x=0;x<n;x++){
                System.out.print(macierzA[i][x] + " ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");

        //Przypisanie wartosci do macierzy B
        for(int i=0;i<n;i++){
            for(int x=0;x<k;x++){
                macierzB[i][x]=FunkcjePomocnicze.getRandomNumber(1,9);
            }
        }
        //Wypisanie macierzy B
        System.out.println("Macierz B: ");
        for(int i=0;i<n;i++){
            for(int x=0;x<k;x++){
                System.out.print(macierzB[i][x] + " ");
            }
            System.out.print("\n");
        }
        System.out.print("\n");

        //mnozenie
        int[][] macierzC = new int[m][k];
        for(int i=0;i<m;i++){
            for(int x=0;x<k;x++){
                int liczba=0;
                    for(int y=0;y<n;y++){
                        liczba+=macierzA[i][y]*macierzB[y][x];
                    }
                macierzC[i][x]=liczba;
            }
        }

        //Wypisanie macierzy AxB
        System.out.println("Macierz AxB: ");
        for(int i=0;i<m;i++){
            for(int x=0;x<k;x++){
                System.out.print(macierzC[i][x] + "\t");
            }
            System.out.print("\n");
        }
    }
}
