package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium02;
import java.util.Scanner;

public class Zadanie2test {
    public static void main( String [] args ) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] tablica = new int[size];
        FunkcjePomocnicze.generuj(tablica,size,-999,999);

        Zad2a.ileNieparzystych(tablica);
        Zad2a.ileParzystych(tablica);
        Zad2b.ileDodatnich(tablica);
        Zad2b.ileUjemnych(tablica);
        Zad2b.ileZerowych(tablica);
        Zad2c.ileMaksymalnych(tablica);
        Zad2d.sumaDodatnich(tablica);
        Zad2d.sumaUjemnych(tablica);
        Zad2e.dlugoscMaksymalnegoCiaguDodatnich(tablica);
        Zad2f.signum(tablica);

        System.out.println("Podaj przedział gdzie fragment tablicy ma byc odwrocony: ");
        int lewy = scanner.nextInt();
        int prawy = scanner.nextInt();
        Zad2g.odwrocFragment(tablica,lewy,prawy);


    }
}
