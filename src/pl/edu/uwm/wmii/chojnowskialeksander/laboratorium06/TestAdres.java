package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium06;

import pl.imiajd.chojnowski.*;

public class TestAdres {
    public static void main(String[] args){
        Adres jeden = new Adres("Kazańska",18,10,"Łomża","18-400");
        Adres dwa = new Adres("Isarstr.",8,"Hannover","30-159");
        jeden.pokaz();
        System.out.println();
        dwa.pokaz();

        System.out.println(jeden.przed(dwa));
    }
}
