package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium06;

import pl.imiajd.chojnowski.*;

public class TestNazwanyPunkt
{
    public static void main(String[] args)
    {
        NazwanyPunkt a = new NazwanyPunkt(4, 12, "lodówka");
        a.show();

        Punkt b = new Punkt(1, 3);
        b.show();

        Punkt c = new NazwanyPunkt(2, 7, "łóżko");
        c.show();

        a = (NazwanyPunkt) c;
        a.show();
    }
}
