package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium04;

import java.util.ArrayList;

public class Zadanie3 {
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, java.util.ArrayList<Integer> b){
        ArrayList<Integer> wynik = Zadanie1.append(a,b);

        //sortowanie bombelkowe
        for(int i=0;i<wynik.size()-1;i++){
            for(int x=0; x<wynik.size()-i-1;x++){
                if(wynik.get(x)>wynik.get(x+1)){
                    int tmp = wynik.get(x);
                    wynik.set(x,wynik.get(x+1));
                    wynik.set(x+1,tmp);
                }
            }
        }
        return wynik;
    }

}
