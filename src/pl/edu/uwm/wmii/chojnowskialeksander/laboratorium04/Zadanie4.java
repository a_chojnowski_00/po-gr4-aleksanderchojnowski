package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium04;

import java.util.ArrayList;

public class Zadanie4 {
    public static ArrayList<Integer> reversed(ArrayList<Integer> a){
        ArrayList<Integer> wynik = new ArrayList<>();
        for(int i=a.size()-1;i>=0;i--){
            wynik.add(a.get(i));
        }
        return wynik;
    }
}
