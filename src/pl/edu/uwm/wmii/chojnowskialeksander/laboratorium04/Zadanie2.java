package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium04;

import java.util.ArrayList;

public class Zadanie2 {
    public static ArrayList<Integer> merge (ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> wynik = new ArrayList<>();
        int dlugosc = Math.max(a.size(), b.size()); //ustalam jak dlugo pętla ma sie 'obracać'

        for(int i=0; i<dlugosc;i++){
            if(i<a.size()) {
                wynik.add(a.get(i));
            }
            if(i<b.size()) {
                wynik.add(b.get(i));
            }
        }
        return wynik;
    }
}
