package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium04;

import java.util.ArrayList;

public class MainTest {
    public static void main (String [] args){
        ArrayList<Integer> liczby =new ArrayList<>();
        liczby.add(6); liczby.add(1); liczby.add(5); liczby.add(7); liczby.add(6); liczby.add(7);
        ArrayList<Integer> liczby2 =new ArrayList<>();
        liczby2.add(8); liczby2.add(0); liczby2.add(4); liczby2.add(3); liczby2.add(1); liczby2.add(9); liczby2.add(9);

        //wypisanie  bazowych list
        System.out.println("liczby : "+liczby);
        System.out.println("liczby2 : "+liczby2);

        //Zadanie1
        System.out.println("funkcja append: "+Zadanie1.append(liczby,liczby2));

        //Zadanie2
        System.out.println("funkcja merge: "+Zadanie2.merge(liczby,liczby2));

        //Zadanie3
        System.out.println("funkcja mergeSorted: "+Zadanie3.mergeSorted(liczby,liczby2));

        //Zadanie4
        System.out.println("funkcja reverse: "+Zadanie4.reversed(liczby));

        //Zadanie5
        Zadanie5.reverse(liczby);
        System.out.println("lista liczby po odwroceniu: "+liczby);
    }
}
