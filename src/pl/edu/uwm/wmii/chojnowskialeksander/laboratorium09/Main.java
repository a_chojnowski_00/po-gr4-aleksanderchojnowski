package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium09;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        Integer [] tablica1 = new Integer[]{2,1,3,7,8,9,5};
        Integer [] tablica2 = new Integer[]{1,2,3,4,5,6,7};
        System.out.println("Czy tablica1 posortowana?: " + ArrayUtil.isSorted(tablica1));
        System.out.println("Czy tablica2 posortowana?: " + ArrayUtil.isSorted(tablica2));
        LocalDate [] tablicaDat1 = new LocalDate[]{LocalDate.parse("2020-10-07"),LocalDate.parse("2020-12-19"),LocalDate.parse("2020-11-11")};
        LocalDate [] tablicaDat2 = new LocalDate[]{LocalDate.parse("2019-03-26"),LocalDate.parse("2020-02-14"),LocalDate.parse("2020-05-17")};
        System.out.println("\nCzy tablicaDat1 posortowana?: " + ArrayUtil.isSorted(tablicaDat1));
        System.out.println("Czy tablicaDat2 posortowana?: " + ArrayUtil.isSorted(tablicaDat2));

        System.out.println("\nLiczba 8 znajduje się na indexie (-1 == brak w tablicy): " + ArrayUtil.binSearch(tablica1,8));
        System.out.println("Data znajduje się na indexie (-1 == brak w tablicy): " + ArrayUtil.binSearch(tablicaDat1, LocalDate.parse("2020-11-01")));
        System.out.println("");

        ArrayUtil.SelectionSort(tablica1);
        System.out.println("Posortowane liczby:");
        for (int i : tablica1){
            System.out.print(i + "\t");
        }
        System.out.println();

        ArrayUtil.SelectionSort(tablicaDat1);
        System.out.println("\nPosortowane daty:");
        for (LocalDate j : tablicaDat1){
            System.out.print(j + "\t");

        }
    }
}
