package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium01;

import java.util.Scanner;

public class Zadanie2_2 {
    public static void main(String[] args) {
        Scanner n = new Scanner(System.in);
        //System.out.print("Ile liczb?");
        int dlugosc = 5; //n.nextInt();
        int[] tablica = new int[dlugosc];
        tablica[0] = 5;
        tablica[1] = -7;
        tablica[2] = 3;
        tablica[3] = -2;
        tablica[4] = -9;
        /*
        //wczytywanie liczb
        for(int i=0;i<dlugosc;i++){
            tablica[i]=n.nextInt();
        }
        */
        int wynik=0;
        for(int i=0;i<dlugosc;i++){
            if(tablica[i]>0){
                wynik+=tablica[i]*2;
            }
        }
        System.out.println(wynik);
    }
}