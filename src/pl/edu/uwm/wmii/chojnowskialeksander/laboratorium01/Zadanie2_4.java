package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium01;

import java.util.Scanner;

public class Zadanie2_4 {
    public static void main(String[] args) {
        Scanner n = new Scanner(System.in);
        //System.out.print("Ile liczb?");
        int dlugosc = 5; //n.nextInt();
        int[] tablica = new int[dlugosc];
        tablica[0] = -5;
        tablica[1] = 7;
        tablica[2] = -3;
        tablica[3] = 2;
        tablica[4] = 9;
        /*
        //wczytywanie liczb
        for(int i=0;i<dlugosc;i++){
            tablica[i]=n.nextInt();
        }
        */
        int min=tablica[0];
        int max=tablica[0];

        for(int i=0;i<dlugosc;i++) {
            if(min>tablica[i])
                min=tablica[i];
            else if(max<=tablica[i])
                max=tablica[i];
        }
        System.out.println("Max: " + max);
        System.out.println("min: " + min);
    }
}