package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium01;

import java.util.Scanner;

public class Zadanie2_3 {
    public static void main(String[] args) {
        Scanner n = new Scanner(System.in);
        //System.out.print("Ile liczb?");
        int dlugosc = 5; //n.nextInt();
        int[] tablica = new int[dlugosc];
        tablica[0] = 5;
        tablica[1] = -7;
        tablica[2] = 0;
        tablica[3] = -2;
        tablica[4] = 9;
        /*
        //wczytywanie liczb
        for(int i=0;i<dlugosc;i++){
            tablica[i]=n.nextInt();
        }
        */
        int zera=0;
        int plus=0;
        int minus=0;
        for(int i=0;i<dlugosc;i++) {
            if (tablica[i] > 0)
                plus++;
            else if (tablica[i] < 0)
                minus++;
            else
                zera++;
        }
        System.out.println("Liczb dodatnich: " + plus);
        System.out.println("Liczb ujemnych: " + minus);
        System.out.println("Zer: " + zera);
    }
}