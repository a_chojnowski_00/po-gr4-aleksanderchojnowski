package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium01;
import java.util.Scanner;

public class Zadanie2_1 {
    public static int silnia(int n) {
        int wynik=1;
        for(int i=1;i<=n;i++)
        {
            wynik *= i;
        }
        return wynik;
    }

    public static void main(String[] args) {
        Scanner n = new Scanner(System.in);
        //System.out.print("Ile liczb?");
        int dlugosc = 5; //n.nextInt();
        int[] tablica = new int[dlugosc];
        tablica[0] = 5;
        tablica[1] = 7;
        tablica[2] = 3;
        tablica[3] = 2;
        tablica[4] = 9;
        /*
        //wczytywanie liczb
        for(int i=0;i<dlugosc;i++){
            tablica[i]=n.nextInt();
        }
        */

        //podpunkt a
        int a = 0;
        for (int i = 0; i < dlugosc; i++) {
            if (tablica[i] % 2 == 1)
                a++;
        }
        System.out.println("Liczb nieparzystych: " + a);

        //podpunkt b
        int b=0;
        for(int i=0;i < dlugosc;i++){
            if(tablica[i]%3==0 && tablica[i]%5!=0)
                b++;
        }
        System.out.println("Liczb |3 i nie |5: " + b);

        //podpunkt c
        int c=0;
        for(int i=0;i < dlugosc;i++){
            for(int x=0;x<dlugosc;x++){
                if(tablica[i]==x*x)
                    c++;
            }
        }
        System.out.println("Liczb ktore sa kwadratami: " + c);

        //podpunkt d
        int d=0;
        for(int i=1;i < dlugosc-1;i++){
            if(tablica[i]<(tablica[i-1]+tablica[i+1]/2))
                d++;
        }
        System.out.println("Liczb spełniajacych warunek d): " + d);

        //popunkt e
        int e=0;
        for(int i=1;i < dlugosc;i++){
            if(tablica[i]>Math.pow(2,i) && tablica[i]<silnia(i))
                e++;
        }
        System.out.println("Liczb spełniajacych warunek e): " + e);

        //podpunkt f
        int f=0;
        for(int i=1;i < dlugosc;i+=2){
            if(tablica[i]%2==0)
                f++;
        }
        System.out.println("Liczb parzyste i nieparzystym indexie: " + f);

        //podpunkt g
        int g=0;
        for(int i=0;i<dlugosc;i++){
            if(tablica[i]>0 && tablica[i]%2!=0)
                g++;
        }
        System.out.println("Liczb nieparzystych nieujemnych: " + g);

        //podpunkt h
        int h=0;
        for(int i=0;i<dlugosc;i++){
            if(Math.abs(tablica[i])<i*i)
                h++;
        }
        System.out.println("Liczb spelniajacych warunek h): " + h);
    }
}
