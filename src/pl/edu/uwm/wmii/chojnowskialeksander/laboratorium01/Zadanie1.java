package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium01;
import java.util.Scanner;

public class Zadanie1 {
    public static int silnia(int n) {
        int wynik=1;
        for(int i=1;i<=n;i++)
        {
            wynik *= i;
        }
        return wynik;
    }

    public static void main ( String [] args ) {
        Scanner n = new Scanner(System.in);
        System.out.print("Ile liczb?");
        int dlugosc = n.nextInt();
        int[] tablica = new int[dlugosc];

        //wczytywanie liczb
        for(int i=0;i<dlugosc;i++){
            tablica[i]=n.nextInt();
        }

        //podpunkt a
        int a=0;
        for(int i=0;i<dlugosc;i++){
            a+=tablica[i];
        }
        System.out.println("wynik dodawania = "+ a);

        //podpunkt b
        int b=1;
        for(int i=0;i<dlugosc;i++){
            b*=tablica[i];
        }
        System.out.println("wynik mnożenia = "+ b);

        //podpunkt c
        int c=0;
        for(int i=0;i<dlugosc;i++){
            c+=Math.abs(tablica[i]);
        }
        System.out.println("wynik dodawania liczb dodatnich = "+ c);

        //podpunkt d
        double d=0;
        for(int i=0;i<dlugosc;i++){
            d+=Math.sqrt(Math.abs(tablica[i]));
        }
        System.out.println("Wynik dodawania pierwiastków z liczb dodatnich = " + d);

        //podpunkt e
        int e=1;
        for(int i=0;i<dlugosc;i++){
            e*=Math.abs(tablica[i]);
        }
        System.out.println("Wynik mnozenia liczb dodatnich = " + e);

        //podpunkt f
        int f=0;
        for(int i=0;i<dlugosc;i++){
            f+=Math.pow(tablica[i],2);
        }
        System.out.println("wynik dodawania kwadratów = "+ f);

        //podpunkt g
        //połączenie podpunktu a i b zatem kod ten sam, napisze samy print

        System.out.println("\nwynik dodawania = "+ a);
        System.out.println("wynik mnozenia = "+ b + "\n");

        //podpunkt h
        int h = 0;
        for(int i=0;i<dlugosc;i++){
            h+=(Math.pow(-1,i+1)*tablica[i]);
        }
        System.out.println("Wynik punktu h = "+ h);

        //podpunkt i
        int ii = 0;
        for(int i=0;i<dlugosc;i++){
            ii+=(Math.pow(-1,i)*tablica[i])/silnia(dlugosc);
        }
        System.out.println("Wynik punktu ii = "+ ii);
    }
}