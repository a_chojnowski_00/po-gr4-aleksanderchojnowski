package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium01;
import java.util.Scanner;

public class Zadanie1_2 {
    public static void main ( String [] args ) {
        Scanner n = new Scanner(System.in);
        System.out.print("Ile liczb?");
        int dlugosc = n.nextInt();
        float[] tablica = new float[dlugosc];

        //wczytywanie liczb
        for(int i=0;i<dlugosc;i++)
        {
            tablica[i]=n.nextFloat();
        }

        //wypisanie liczb
        for(int i=1;i<dlugosc;i++){
            System.out.println("tablica["+i+"]="+tablica[i]);
        }
        System.out.println("tablica[0]="+tablica[0]);
    }
}
