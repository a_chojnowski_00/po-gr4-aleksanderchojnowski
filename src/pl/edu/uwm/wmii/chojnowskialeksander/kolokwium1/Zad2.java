package pl.edu.uwm.wmii.chojnowskialeksander.kolokwium1;

public class Zad2 {
    public static String delete(String str, char c){
        String wynik = "";
        for(int i=0;i<str.length();i++) {
            char litera = str.charAt(i);
            if (litera == 'a' ||  litera== 'e'|| litera == 'y'|| litera == 'u'|| litera == 'i'|| litera == 'o' || litera == 'A'|| litera == 'E'|| litera == 'I'|| litera == 'O'|| litera == 'U'|| litera == 'Y') {
                wynik+= c;
            }
            else {
                wynik += litera;
            }
        }
        return wynik;
    }
}
