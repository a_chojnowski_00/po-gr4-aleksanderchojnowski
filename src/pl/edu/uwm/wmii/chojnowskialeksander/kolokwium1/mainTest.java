package pl.edu.uwm.wmii.chojnowskialeksander.kolokwium1;

import java.util.Scanner;

public class mainTest {
    public static void main(String[] args){
        Scanner n = new Scanner(System.in);
        System.out.print("Ile liczb?");
        int dlugosc = n.nextInt();
        int[] tablica = new int[dlugosc];

        //wczytywanie liczb
        for(int i=0;i<dlugosc;i++){
            tablica[i]=n.nextInt();
        }


        //ZADANIE1
        Zad1.counter(tablica);

        //ZADANIE2
        String nazwisko = "chojnowski";
        System.out.println(Zad2.delete(nazwisko,'x'));

    }
}
