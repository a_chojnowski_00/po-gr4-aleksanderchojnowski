package pl.edu.uwm.wmii.chojnowskialeksander.kolokwium1;

public class Zad1 {
    public static void counter(int[] tab){
        int wieksze=0;
        int mniejsze=0;
        int rowne=0;

        for(int elem : tab){
            if(elem > 5)
                wieksze++;
            else if(elem < -5)
                mniejsze++;
            else if(elem == 5)
                rowne++;
        }
        System.out.println("Liczby większych od 5: " + wieksze);
        System.out.println("Liczby mniejszych od -5: " + mniejsze);
        System.out.println("Liczby równych 5: " + rowne);
    }
}
