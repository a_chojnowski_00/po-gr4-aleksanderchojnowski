package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium10;
import java.util.*;
import static java.lang.StrictMath.sqrt;

public class Main {
    public static <T> void redukuj(LinkedList<T> pracownicy, int n) {
        for (int i = 0; i < pracownicy.size(); i += (n - 1)) {
            pracownicy.remove(i);
        }
        System.out.println(pracownicy);
    }
    public static <T> void odwroc(LinkedList<T> lista){
        LinkedList<T> odwrocona = new LinkedList<T>();
        for(int i= lista.size()-1; i>=0; i--){
            odwrocona.add(lista.get(i));
        }
        System.out.println(odwrocona);
    }
    public static void stos(Stack<String> stos){
        Stack<String> odwrocony = new Stack<String>();
        int x = 0;
        for(int i=0; i<stos.size(); i++){
            if(stos.get(i).contains(".")){
                if(x==0){
                    for(int j=i; j>i-3; j--){
                        odwrocony.push(stos.get(j));
                    }
                }else{
                    for(int j=i; j>i-4; j--){
                        odwrocony.push(stos.get(j));
                    }
                }
                x=1;
            }
        }
        System.out.println(odwrocony);
    }
    public static void nieujemna(){
        Scanner sc = new Scanner(System.in);
        Stack<Integer> stos = new Stack<>();
        System.out.println("Podaj liczbe: ");
        int x = sc.nextInt();
        String temp = Integer.toString(x);
        int[] tab = new int[temp.length()];
        for (int i = 0; i < temp.length(); i++) {
            tab[i] = temp.charAt(i) - '0';
        }
        for (int s : tab){
            stos.push(s);
        }
        for (int i=0;i<tab.length;i++){
            System.out.println(stos.pop());
        }
    }
    public static void sito(){
        Set <Integer> pierwsze = new HashSet<>();
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe:");
        int n = sc.nextInt();
        int i;
        int num=0;
        for ( i = 1; i <= n; i++) {
            int counter=0;
            for(num=i; num>=1; num--) {
                if(i%num==0) {
                    counter = counter + 1;
                }
            }
            if (counter ==2) {
                pierwsze.add(i);
            }
        }
        System.out.println("\n\n");
        for (i=2; i<=n;i++){
            pierwsze.add(i);

        }
        for (int g : pierwsze){
            System.out.println(g);
        }
        List<Integer> tmp = new ArrayList<>(pierwsze);
        for (i=2; i<sqrt(n);i++){
            {
                for (int j=0;j< tmp.size();j++){
                    if (tmp.get(j)%i==0 && tmp.get(j)!=i){
                        tmp.remove(j);
                    }
                }
            }
        }
        Set<Integer> primes = new HashSet<>(tmp);
        for (int p : primes){
            System.out.println(p);
        }
    }
    public static <E extends Iterable<?>> void print (E o ){
        Iterator <?> iterator = o.iterator();
        while(iterator.hasNext()) {
            System.out.println( iterator.next());
            if(iterator.hasNext()){
                System.out.println(", ");
            }
        }
    }

    public static void main(String[] args) {
        LinkedList<String> pracownicy = new LinkedList<String>();
        pracownicy.add("Chojnowski");
        pracownicy.add("Nowak");
        pracownicy.add("Kowalski");
        pracownicy.add("Romanowski");
        pracownicy.add("Krawężnik");
        LinkedList<Integer> pracownicyLiczby = new LinkedList<Integer>();
        pracownicyLiczby.add(1);
        pracownicyLiczby.add(2);
        pracownicyLiczby.add(3);
        pracownicyLiczby.add(4);
        pracownicyLiczby.add(5);
        System.out.println("Podaj n:");
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        redukuj(pracownicy,n);
        redukuj(pracownicyLiczby,n);
        LinkedList<String> lista = new LinkedList<String>();
        lista.add("Chojnowski");
        lista.add("Nowak");
        lista.add("Kowalski");
        lista.add("Romanowski");
        lista.add("Krawężnik");
        LinkedList<Integer> listaLiczba = new LinkedList<Integer>();
        listaLiczba.add(1);
        listaLiczba.add(2);
        listaLiczba.add(3);
        listaLiczba.add(4);
        listaLiczba.add(5);
        odwroc(lista);
        odwroc(listaLiczba);
        Stack<String> str = new Stack<String>();
        str.push("Ala");
        str.push("ma");
        str.push("kota.");
        str.push("Jej");
        str.push("kot");
        str.push("lubi");
        str.push("myszy.");
        stos(str);
        nieujemna();
        Set <Integer> pierwsze = new HashSet<>();
        sito();
        LinkedList<Integer> liczby = new LinkedList<>();
        liczby.add(1);
        liczby.add(2);
        liczby.add(3);
        liczby.add(4);
        liczby.add(5);
        liczby.add(6);
        print(liczby);
    }
}
