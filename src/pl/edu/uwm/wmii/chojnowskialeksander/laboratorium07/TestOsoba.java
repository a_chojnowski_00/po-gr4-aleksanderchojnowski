package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium07;

import pl.imiajd.chojnowski02.*;
import java.time.LocalDate;

public class TestOsoba
{
    public static void main(String[] args) {
        Pracownik a=new Pracownik("Chojnowski", new String[]{"Aleksander"}, LocalDate.of(2000, 3,26), true, 3000, LocalDate.of(2020, 11, 11));
        Student b=new Student("Nowak", new String[]{"Ewa","Urszula"}, LocalDate.of(1999, 2,2), false, "Informatyka", 4.2);
        System.out.println(a.getNazwisko());
        for(String i : a.getImiona())
        {
            System.out.print(i+"\n");
        }
        System.out.println(a.getDataUrodzenia());
        System.out.println(a.getPlec());
        System.out.println(a.getPobory());
        System.out.println(a.getDataZatrudnienia());
        System.out.println(a.getOpis());
        for(String i : b.getImiona())
        {
            System.out.print(i+", ");
        }
        System.out.println(b.getKierunek());
        System.out.println(b.getSredniaOcen());
        b.getSredniaOcen();
        b.setSredniaOcen(4.8);
        System.out.println(b.getOpis());
    }
}






