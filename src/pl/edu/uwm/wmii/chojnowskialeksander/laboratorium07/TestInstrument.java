package pl.edu.uwm.wmii.chojnowskialeksander.laboratorium07;

import pl.imiajd.chojnowski02.Flet;
import pl.imiajd.chojnowski02.Fortepian;
import pl.imiajd.chojnowski02.Instrument;
import pl.imiajd.chojnowski02.Skrzypce;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrument {
    public static void main(String[] args){
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Flet("Yamaha", LocalDate.of(1999,1,1)));
        orkiestra.add(new Fortepian("Panasonic", LocalDate.of(1536,3,3)));
        orkiestra.add(new Skrzypce("Nissan", LocalDate.of(365,6,2)));
        orkiestra.add(new Flet("Samsung", LocalDate.of(514,5,7)));
        orkiestra.add(new Skrzypce("Audi", LocalDate.of(1654,1,31)));
        for(int i = 0; i<5; i++){
            System.out.println(orkiestra.get(i).toString());
            System.out.println(orkiestra.get(i).getDzwiek());
        }
    }
}
